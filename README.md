# Mini Project 3: Create an S3 Bucket using CDK with AWS CodeWhisperer. 

## Project Description

In this project, I aim to create S3 bucket using AWS CDK but fail at the deploy step. I use CodeWhisperer to generate CDK code, and add bucket properties like versioning and encryption. The tools used for this projects are AWS CodeCatalyst, AWS Cloud 9 Environment, and CodeWhisperer which helps to generates the code.

## The Steps I followed:

1. Sign in [CodeCatalyst](https://codecatalyst.aws/explore). Create a empty project.
2. Inside the project, create a new environment with AWS Cloud 9, CodeCatalyst will then direct to the new dev environment
3. Navigate to AWS IAM and create a new user with the following permissions: `IAMFullAccess`, `AmazonS3FullAccess` and `AWSLambda_FullAccess`. Then add the inline policies `Cloud Formation`, `Systems Manager` (all access), go back to the add-permissions page, reflesh, and add these two polices.
4. Navigate to `Security Credentials` page for this new user, create access key under selected user and download the csv file to store the access key.
5. Go back to Cloud 9 environment tab, in the terminal, type `aws configure`.
6. Type `mkdir hello-cdk` to create a new project directory. Then `cd` to this directory and type `cdk init app --language=typescript` to create the project template.
7. Enable CodeWhisperer to generate code for a S3 Bucket (reference: https://docs.aws.amazon.com/codewhisperer/latest/userguide/cloud9-setup.html)
- From inside your existing AWS Cloud9 environment, choose the AWS logo on the left edge of the window. A panel will expand rightward.
- In the lower part of the panel, under Developer tools, open the CodeWhisperer dropdown.
- Choose Enable CodeWhisperer. 
8. Utilize CodeWhisperer to generate code for a S3 Bucket:
- in `\lib\mini-proj-3-stack.ts`, use the prompt `//make an S3 bucket that enables versioning and encrption`.
- Under `\bin\mini-proj-3.ts`, use the prompt `//add necessary variables so that a S3 bucket is created and is deployed correctly`.
10. type `npm run build` in the terminal.
11. then type `cdk synth` in the terminal
12. Finaly, use `cdk deploy` deploy this stack to default AWS account/region, this is where I failed: as the screenshots shown below: the error suggests "HelloCdkStack: SSM parameter /cdk-bootstrap/hnb659fds/version not found." and suggests running cdk bootstrap.
13. Try `cdk bootstrap` in the terminal, but it also fails, the error suggesting that the stack might need to be manually deleted. 
13. I then go to AWS CloudFormation console, the status for stack CDKToolkit shows as "ROLLBACK_COMPLETE", I seeks help on slack channels and internet but unfortunately didn't find a good way to solve this issue.

 ## screenshots for Utilizing CodeWhisperer 
 * use CodeWhisperer to generate S3 bucket code with prompt //make an S3 bucket that enables versioning and encrption
![](CodeWhisperer1.png)
 * use CodeWhisperer to generate the necessary variables
 ![](CodeWhisperer2.png)

 ## screenshots for Deploying failure
![](deploy_fail.png)
![](bootstrap_fail.png)
